# xml-dtd
An attempt to make a DTD making it easier to make them in XML.
## Using
When complete, the code below will make a XML Document Type Definition when compiled.
(compiler not yet included)
### Variables
Variables are global fixed values intending to make entities easier -
you can literally use the `<entity>` tag - so in the code,
instead of typing `Pneumonoultramicroscopicsilicovolcanoconiosis` 70
times (or pasting it, for that matter) you could make an `&#010;`. or
`&lung;`.

> Above, I would have put the 189,819 letter word for Titin, but that
> would be too big. :unamused:

#### Creating
Use the `<variable>` tag to make a variable. Variables are
*always* set, as the DTD is fixed; there are no functions or
sections. (A JavaScript editon of this may be included in future,
supporting JavaScript variables.)  

| Name    | Description                        | Example                     | Notes                                         |
|---------|------------------------------------|-----------------------------|-----------------------------------------------|
| `name`  | Defines the name of a variable.    | `<variable name="VAR_name">` | Case-sensitive.                               |
| `value` | Defines the value of the variable. | `<variable value="10">`     | Can be both a string and numerical character. |

##### Always remember to include *both* the `name` and `value` attributes.
#### Using variables
Variables can be used by using the `$` before the name of the variable
defined in the `name` attribute. The following is an example:
```xml
<attribute name="$MY_VARIABLE">
```
##### The above example is *not* how to make an attribute! See the Attribute section below!
### Elements
#### Explanation
The element tag is a container. This means it it like a `<div>` element.
However, it is only allowed to contain the `<attribute>` tag specified
in xml-dtd, rather than any element like a `<div>`. The element tag is
created by initializing an `<element>` tag and a `name` attribute,
creating an `<element>` tag.
#### Usage
The element tag has one attribute: `name`. The attributes are described
in the container with `<attribute>` tags, so the only attribute is an
identifier; the name.
> There there are plans for a `variable` attribute, linking to the element.
> This may be used later, for binding attributes.

> Try not to set the inside text to anything with the `value` attribute; if you
> need a datepicker, for example, get browsers to show a datepicker when that
> attribute is seen. Try not to insert `<input type="datepicker">`.

##### The name attribute is REQUIRED!
#### Attributes
| Name   | Description                                          | Usage                    | Notes                |
|--------|------------------------------------------------------|--------------------------|----------------------|
| name   | Sets the name of the element.                        | `name="myElement"`       |                      |
| value  | Sets the inner value of the element.                 | `value="<input>"`        | Not recommended      |
| omit   | Sets if the tag is not required (true = yes, false = no) | `omit` OR `omit="false"` | True by default      |
| type   | Sets the tag type (see Tag Types below)              | `type="container"`       | Container by default |
| parent | Set the required parent element                      | `parent="head"`          |                      |

#### Tag Types
These are the values for the `type` attribute.  
* container  
* meta  
* object  

#### Comparisons
As some of the attributes above may be weird, here's a chart to explain
them.
* **Name** sets the name of the tag. If the name is `moo`, then
```html
<moo></moo>
```
would be how to use it.
* **Value** sets what is inside the attribute. If you know JavaScript or
jQuery, you should know the `innerHtml` and `html` fields of an element.
This is similar &#8212; it sets the inner HTML of the field.
* **Omit** sets if you *must* have the element; if false, you MUST have
the element, similar to HTML's `<title>` element.
* **Type** sets what type of element it is.
  * `container` means it can contain text and other elements.
  * `meta` sets internal data and should not be visible.
  * `object` is similar to `container`, but can't contain anything.
* **Parent** sets what element *must* be the parent; similar to how
`<title>` must be inside the `<head>` element.

### Attributes
Attributes are put inside a `<element>` tag, adding an attribute for
that element.

#### Usage
```xml
<element name="myElement">
  <attribute name="myAttribute" content="string">
</element>
```
would mean
```html
<myElement myAttribute="myString"></myElement>
```
| Name     | Description                                            | Usage                           | Notes             |
|----------|--------------------------------------------------------|---------------------------------|-------------------|
| name     | Sets the name of the attribute.                        | `name="myAttribute"`            |                   |
| value    | If nothing is set, the internal value.                 | `value="hello world!"`          |                   |
| required | Sets if it is required.                                | `required` OR `required="true"` | False by default  |
| type     | Sets the type of attribute (see Attribute Types below) | `type="string"`                 | String by default |

#### Attribute Types
Elements aren't the only ones with types! Attribute types specify
what data should be stored there. These types are almost 100%
self-explanitory, so consult a developer's dictionary if you're
stuck.
* string
* numerical
* anything
* javascript
* css

### More to be written later.
